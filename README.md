## Instalation

```
bundle install
```

## Changes

The style guide config file is in public folder. It is used by most of the company projects.

## Deployment

```
cap production deploy
```