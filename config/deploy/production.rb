role :app, %w[style-guide.elevatecompany.eu]
role :web, %w[style-guide.elevatecompany.eu]
role :db,  %w[style-guide.elevatecompany.eu]
set :deploy_to, '/home/web/style-guide/'

server 'style-guide.elevatecompany.eu', roles: %w[web app], ssh_options: { user: 'web', port: 2201 }
