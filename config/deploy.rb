lock '~> 3.14.1'

set :application, 'style-guide'
set :repo_url, 'git@bitbucket.org:elevatebg/style-guide.git'
set :branch, ENV['branch'] || 'master'
set :rvm_ruby_version, IO.read('.ruby-version')
set :linked_dirs, %w[log]
set :keep_releases, 3
